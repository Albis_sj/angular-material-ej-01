import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FoodNodeI } from '../interfaces/datos.interface';



@Injectable({
  providedIn: 'root'
})
export class TreeViewService {

  constructor(public http: HttpClient) { }

  getInfo(): Observable<FoodNodeI[]>{
    return this.http.get<FoodNodeI[]>('./assets/data.json')
  }
}
